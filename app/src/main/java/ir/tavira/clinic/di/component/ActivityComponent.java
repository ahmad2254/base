/*
 * Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://mindorks.com/license/apache-v2
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package ir.tavira.clinic.di.component;

import dagger.Component;
import ir.tavira.clinic.di.module.ActivityModule;
import ir.tavira.clinic.di.scope.PerActivity;
import ir.tavira.clinic.ui.MainActivity.MainActivity;
import ir.tavira.clinic.ui.MainActivity.Main.MainFragment;
import ir.tavira.clinic.ui.base.BaseActivity;

/**
 * Created by janisharali on 27/01/17.
 */

@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = ActivityModule.class)
public interface ActivityComponent {
    void inject(BaseActivity activity);
    void inject(MainActivity activity);
    ////fragments
    void inject(MainFragment fragment);
}
