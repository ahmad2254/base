/*
 * Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://mindorks.com/license/apache-v2
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package ir.tavira.clinic.di.module;

import android.content.Context;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import dagger.Module;
import dagger.Provides;
import io.reactivex.disposables.CompositeDisposable;
import ir.tavira.clinic.di.qualifier.ActivityContext;
import ir.tavira.clinic.di.scope.PerActivity;
import ir.tavira.clinic.ui.MainActivity.Main.MainFragmentMvpPresenter;
import ir.tavira.clinic.ui.MainActivity.Main.MainFragmentMvpView;
import ir.tavira.clinic.ui.MainActivity.Main.MainFragmentPresenter;
import ir.tavira.clinic.ui.MainActivity.MainMvpPresenter;
import ir.tavira.clinic.ui.MainActivity.MainMvpView;
import ir.tavira.clinic.ui.MainActivity.MainPresenter;
import ir.tavira.clinic.utils.rx.AppSchedulerProvider;
import ir.tavira.clinic.utils.rx.SchedulerProvider;

/**
 * Created by janisharali on 27/01/17.
 */

@Module
public class ActivityModule {

    private AppCompatActivity mActivity;

    public ActivityModule(AppCompatActivity activity) {
        this.mActivity = activity;
    }

    @Provides
    @ActivityContext
    Context provideContext() {
        return mActivity;
    }

    @Provides
    AppCompatActivity provideActivity() {
        return mActivity;
    }

    @Provides
    CompositeDisposable provideCompositeDisposable() {
        return new CompositeDisposable();
    }

    @Provides
    SchedulerProvider provideSchedulerProvider() {
        return new AppSchedulerProvider();
    }

    @Provides
    LinearLayoutManager provideLinearLayoutManager(AppCompatActivity activity) {
        return new LinearLayoutManager(activity);
    }

    ////////////peresenters
    @Provides
    @PerActivity
    MainMvpPresenter<MainMvpView> provideMainPresenter(
            MainPresenter<MainMvpView> presenter) {
        return presenter;
    }

    @Provides
    MainFragmentMvpPresenter<MainFragmentMvpView> provideMainFragmentMvpPresenter(
            MainFragmentPresenter<MainFragmentMvpView> presenter) {
        return presenter;
    }

}
