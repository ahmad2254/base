/*
 * Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://mindorks.com/license/apache-v2
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package ir.tavira.clinic.data.network;


import com.google.gson.GsonBuilder;
import com.rx2androidnetworking.Rx2AndroidNetworking;

import org.json.JSONException;
import org.json.JSONObject;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Single;
import ir.tavira.clinic.data.network.model.RegisterRequest;
import ir.tavira.clinic.data.network.model.RegisterResponse;

/**
 * Created by janisharali on 28/01/17.
 */

@Singleton
public class AppApiHelper implements ApiHelper {

    private ApiHeader mApiHeader;

    @Inject
    public AppApiHelper(ApiHeader apiHeader) {
        mApiHeader = apiHeader;
    }

    @Override
    public ApiHeader getApiHeader() {
        return mApiHeader;
    }

    @Override
    public Single<RegisterResponse> doServerRegisterApiCall(RegisterRequest request) {
        String token="0";
        if( mApiHeader.getProtectedApiHeader().getFirebase_token()!=null)
            token= mApiHeader.getProtectedApiHeader().getFirebase_token();
        return Rx2AndroidNetworking.post(ApiEndPoint.ENDPOINT_USER_REGISTER)
                .addBodyParameter("name", request.getName())
                .addBodyParameter("family", request.getFamily())
                .addBodyParameter("mobile", request.getMobile())
                .addBodyParameter("password", request.getPassword())
                .addBodyParameter("national_id", request.getNational_id())
                .addBodyParameter("birthday", request.getBirthday())
                .addBodyParameter("type", request.getType())
                .addBodyParameter("firebase_token",token)
                .build()
                .getObjectSingle(RegisterResponse.class);
    }
}

