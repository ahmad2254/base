/*
 * Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://mindorks.com/license/apache-v2
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */
package ir.tavira.clinic.data;

import android.content.Context;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Single;
import ir.tavira.clinic.data.network.ApiHeader;
import ir.tavira.clinic.data.network.ApiHelper;
import ir.tavira.clinic.data.network.model.RegisterRequest;
import ir.tavira.clinic.data.network.model.RegisterResponse;
import ir.tavira.clinic.data.prefs.PreferencesHelper;
import ir.tavira.clinic.di.qualifier.ApplicationContext;


/**
 * Created by janisharali on 27/01/17.
 */
@Singleton
public class AppDataManager implements DataManager {

    private static final String TAG = "AppDataManager";

    private final Context mContext;
    private final PreferencesHelper mPreferencesHelper;
    private final ApiHelper mApiHelper;

    @Inject
    public AppDataManager(@ApplicationContext Context context, PreferencesHelper preferencesHelper,
                          ApiHelper apiHelper) {
        mContext = context;
        mPreferencesHelper = preferencesHelper;
        mApiHelper = apiHelper;
    }

    @Override
    public ApiHeader getApiHeader() {
        return mApiHelper.getApiHeader();
    }

    @Override
    public Single<RegisterResponse> doServerRegisterApiCall(RegisterRequest request) {
        return mApiHelper.doServerRegisterApiCall(request);
    }

    @Override
    public String getAccessToken() {
        return mPreferencesHelper.getAccessToken();
    }

    @Override
    public void setAccessToken(String accessToken) {
        mPreferencesHelper.setAccessToken(accessToken);
        mApiHelper.getApiHeader().getProtectedApiHeader().setAccessToken(accessToken);
    }

    @Override
    public void setFireBaseToken(String firebase_token) {
        mPreferencesHelper.setFireBaseToken(firebase_token);
        mApiHelper.getApiHeader().getProtectedApiHeader().setFirebase_token(firebase_token);
    }
    @Override
    public String getFireBaseToken() {
        return mPreferencesHelper.getFireBaseToken();
    }

    @Override
    public void setSetting_Title(String title) {
        mPreferencesHelper.setSetting_Title(title);
    }

    @Override
    public void setSetting_aboute_us(String abouteUs) {
        mPreferencesHelper.setSetting_aboute_us(abouteUs);
    }

    @Override
    public void setSetting_aboute_us_short(String aboute_us_short) {
        mPreferencesHelper.setSetting_aboute_us_short(aboute_us_short);
    }

    @Override
    public void setSetting_contact_us(String contact_us) {
        mPreferencesHelper.setSetting_contact_us(contact_us);
    }

    @Override
    public void setSetting_contact_us_short(String contact_us_short) {
        mPreferencesHelper.setSetting_contact_us_short(contact_us_short);
    }

    @Override
    public void setSetting_Rules(String rules) {
        mPreferencesHelper.setSetting_Rules(rules);
    }

    @Override
    public String getSetting_Title() {
        return mPreferencesHelper.getSetting_Title();
    }

    @Override
    public String getSetting_aboute_us() {
        return mPreferencesHelper.getSetting_aboute_us();
    }

    @Override
    public String getSetting_aboute_us_short() {
        return mPreferencesHelper.getSetting_aboute_us_short();
    }

    @Override
    public String getSetting_contact_us() {
        return mPreferencesHelper.getSetting_contact_us();
    }

    @Override
    public String getSetting_contact_us_short() {
        return mPreferencesHelper.getSetting_contact_us_short();
    }

    @Override
    public String getSetting_Rules() {
        return mPreferencesHelper.getSetting_Rules();
    }

    @Override
    public void setCurentName(String name) {
        mPreferencesHelper.setCurentName(name);
    }

    @Override
    public String getReagent_code() {
        return mPreferencesHelper.getReagent_code();
    }

    @Override
    public void setReagent_code(String reagent_code) {
        mPreferencesHelper.setReagent_code(reagent_code);
    }

    @Override
    public void setCurentUserFamily(String family) {
        mPreferencesHelper.setCurentUserFamily(family);
    }

    @Override
    public void setCurentUserPhoneNumber(String phoneNumber) {
        mPreferencesHelper.setCurentUserPhoneNumber(phoneNumber);
    }

    @Override
    public void setCurentUserAddress(String address) {
        mPreferencesHelper.setCurentUserAddress(address);
    }

    @Override
    public void setCurentUserTell(String tell) {
        mPreferencesHelper.setCurentUserTell(tell);
    }

    @Override
    public void setCurentUserPostalCode(String postalCode) {
        mPreferencesHelper.setCurentUserPostalCode(postalCode);
    }

    @Override
    public void setCurentUserNational_Id(String national_id) {
        mPreferencesHelper.setCurentUserNational_Id(national_id);
    }

    @Override
    public void setCurentUserBirthDay(String birthDay) {
        mPreferencesHelper.setCurentUserBirthDay(birthDay);
    }

    @Override
    public String getCurentUserNational_Id() {
        return mPreferencesHelper.getCurentUserNational_Id();
    }

    @Override
    public String getCurentUserBirthDay() {
        return mPreferencesHelper.getCurentUserBirthDay();
    }

    @Override
    public void setCurentUserPass(String password) {
        mPreferencesHelper.setCurentUserPass(password);
    }

    @Override
    public String getCurentName() {
        return mPreferencesHelper.getCurentName();
    }

    @Override
    public String getCurentUserFamily() {
        return mPreferencesHelper.getCurentUserFamily();
    }

    @Override
    public String getCurentUserWallet() {
        return mPreferencesHelper.getCurentUserWallet();
    }

    @Override
    public String getCurentUserPhoneNumber() {
        return mPreferencesHelper.getCurentUserPhoneNumber();
    }

    @Override
    public String getCurentUserAddress() {
        return mPreferencesHelper.getCurentUserAddress();
    }

    @Override
    public String getCurentUserTell() {
        return mPreferencesHelper.getCurentUserTell();
    }

    @Override
    public String getCurentUserPostalCode() {
        return mPreferencesHelper.getCurentUserPostalCode();
    }

    @Override
    public String getCurentUserPass() {
        return mPreferencesHelper.getCurentUserPass();
    }

    @Override
    public int getCurrentUserLoggedInMode() {
        return mPreferencesHelper.getCurrentUserLoggedInMode();
    }

    @Override
    public void setCurrentUserLoggedInMode(LoggedInMode mode) {
        mPreferencesHelper.setCurrentUserLoggedInMode(mode);
    }

    @Override
    public Long getCurrentUserId() {
        return mPreferencesHelper.getCurrentUserId();
    }

    @Override
    public void setCurrentUserId(String userId) {
        mPreferencesHelper.setCurrentUserId(userId);
    }

    @Override
    public void setCurentUserWallet(String wallet) {
        mPreferencesHelper.setCurentUserWallet(wallet);
    }

    @Override
    public String getCurrentUserName() {
        return mPreferencesHelper.getCurrentUserName();
    }

    @Override
    public void setCurrentUserName(String userName) {
        mPreferencesHelper.setCurrentUserName(userName);
    }

    @Override
    public String getCurrentUserEmail() {
        return mPreferencesHelper.getCurrentUserEmail();
    }

    @Override
    public void setCurrentUserEmail(String email) {
        mPreferencesHelper.setCurrentUserEmail(email);
    }

    @Override
    public String getCurrentUserProfilePicUrl() {
        return mPreferencesHelper.getCurrentUserProfilePicUrl();
    }

    @Override
    public void setCurrentUserProfilePicUrl(String profilePicUrl) {
        mPreferencesHelper.setCurrentUserProfilePicUrl(profilePicUrl);
    }

    @Override
    public void updateApiHeader(String userId, String accessToken) {
        mApiHelper.getApiHeader().getProtectedApiHeader().setAccessToken(accessToken);
    }

    @Override
    public void updateUserInfo(
            String accessToken,
            String userId,
            LoggedInMode loggedInMode,
            String userName,
            String email,
            String profilePicPath) {
        setAccessToken(accessToken);
        //setCurrentUserId(userId);
        setCurrentUserLoggedInMode(loggedInMode);
        setCurrentUserName(userName);
        setCurrentUserEmail(email);
        setCurrentUserProfilePicUrl(profilePicPath);
        updateApiHeader(userId, accessToken);
    }

    @Override
    public void setUserAsLoggedOut() {
        updateUserInfo(
                null,
                null,
                DataManager.LoggedInMode.LOGGED_IN_MODE_LOGGED_OUT,
                null,
                null,
                null);
    }
}
