/*
 * Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://mindorks.com/license/apache-v2
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package ir.tavira.clinic.data.network;


import ir.tavira.clinic.utils.Config;

/**
 * Created by amitshekhar on 01/02/17.
 */

public final class ApiEndPoint {


    public static final String ENDPOINT_SERVER_LOGIN = Config.BASE_URL
            + "login";
    public static final String ENDPOINT_USER_REGISTER = Config.BASE_URL
            + "register";
    public static final String ENDPOINT_USER_CONFIRM = Config.BASE_URL
            + "confirm";

    public static final String ENDPOINT_USER_AGAIN = Config.BASE_URL
            + "again";

    public static final String ENDPOINT_SERVICE_GETSERVISES = Config.BASE_URL
            + "services";

    public static final String ENDPOINT_SLIDER_GETSLIDER = Config.BASE_URL
            + "sliders";
    public static final String ENDPOINT_LOCATION_GETLOCATIONS = Config.BASE_URL
            + "places";
    public static final String ENDPOINT_USER_GETDETAILS = Config.BASE_URL
            + "user";

    public static final String ENDPOINT_USER_UPDATEPROFILE = Config.BASE_URL
            + "user-update";

    public static final String ENDPOINT_SETTING_GETSETTINGS = Config.BASE_URL
            + "setting";

    public static final String ENDPOINT_CONTACT_SENDMESSAGE = Config.BASE_URL
            + "contact";

    public static final String ENDPOINT_CONTACT_GETMESSAGES = Config.BASE_URL
            + "news";

    public static final String ENDPOINT_ORDER_ORDERLIST = Config.BASE_URL
            + "order-list";
    public static final String ENDPOINT_ORDER = Config.BASE_URL
            + "order";
    public static final String ENDPOINT_ORDER_DETAIL= Config.BASE_URL
            + "order-info";

    public static final String ENDPOINT_ORDER_REPEAT= Config.BASE_URL
            + "order-repeat";

    public static final String ENDPOINT_ORDER_ACCEPT= Config.BASE_URL
            + "order-accept";
    public static final String ENDPOINT_ORDER_CANCEL= Config.BASE_URL
            + "order-revoke";

    public static final String ENDPOINT_ORDER_SCORE= Config.BASE_URL
            + "order-score";

    public static final String ENDPOINT_USER_GALLERY_CV= Config.BASE_URL
            + "gallery";

    public static final String ENDPOINT_USER_RESETPASSWORD = Config.BASE_URL
            + "reset-password";

    public static final String ENDPOINT_WALLET = Config.BASE_URL
            + "wallet";

    private ApiEndPoint() {
        // This class is not publicly instantiable
    }

}
