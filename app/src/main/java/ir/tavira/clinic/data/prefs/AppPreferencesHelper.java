/*
 * Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://mindorks.com/license/apache-v2
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package ir.tavira.clinic.data.prefs;

import android.content.Context;
import android.content.SharedPreferences;

import javax.inject.Inject;
import javax.inject.Singleton;

import ir.tavira.clinic.data.DataManager;
import ir.tavira.clinic.di.qualifier.ApplicationContext;
import ir.tavira.clinic.di.qualifier.PreferenceInfo;
import ir.tavira.clinic.utils.AppConstants;

/**
 * Created by janisharali on 27/01/17.
 */

@Singleton
public class AppPreferencesHelper implements PreferencesHelper {

    private static final String PREF_KEY_USER_LOGGED_IN_MODE = "PREF_KEY_USER_LOGGED_IN_MODE";
    private static final String PREF_KEY_CURRENT_USER_ID = "PREF_KEY_CURRENT_USER_ID";
    private static final String PREF_KEY_CURRENT_USER_NAME = "PREF_KEY_CURRENT_USER_NAME";
    private static final String PREF_KEY_CURRENT_USER_EMAIL = "PREF_KEY_CURRENT_USER_EMAIL";
    private static final String PREF_KEY_CURRENT_USER_National_Id = "PREF_KEY_CURRENT_USER_National_Id";
    private static final String PREF_KEY_CURRENT_NAME = "PREF_KEY_CURRENT_NAME";
    private static final String PREF_KEY_CURRENT_USER_FAMILY = "PREF_KEY_CURRENT_USER_FAMILY";
    private static final String PREF_KEY_CURRENT_USER_PHONENUMBER = "PREF_KEY_CURRENT_USER_PHONENUMBER";
    private static final String PREF_KEY_CURRENT_USER_TELL = "PREF_KEY_CURRENT_USER_TELL";
    private static final String PREF_KEY_CURRENT_USER_ADDRESS = "PREF_KEY_CURRENT_USER_ADDRESS";
    private static final String PREF_KEY_CURRENT_USER_PASSWORD = "PREF_KEY_CURRENT_USER_PASSWORD";
    private static final String PREF_KEY_CURRENT_USER_WALLET = "PREF_KEY_CURRENT_USER_WALLET";
    private static final String PREF_KEY_CURRENT_USER_POSTALCODE = "PREF_KEY_CURRENT_USER_POSTALCODE";
    private static final String PREF_KEY_CURRENT_USER_BIRTHDAY = "PREF_KEY_CURRENT_USER_BIRTHDAY";
    private static final String PREF_KEY_CURRENT_USER_REAGETNT_CODE = "PREF_KEY_CURRENT_USER_REAGETNT_CODE";
    private static final String PREF_KEY_CURRENT_USER_PROFILE_PIC_URL
            = "PREF_KEY_CURRENT_USER_PROFILE_PIC_URL";
    private static final String PREF_KEY_ACCESS_TOKEN = "PREF_KEY_ACCESS_TOKEN";
    private static final String PREF_KEY_ACCESS_FIREBASETOKEN = "PREF_KEY_ACCESS_FIREBASETOKEN";


    private static final String PREF_KEY_SETTING_TITLE = "PREF_KEY_SETTING_TITLE";
    private static final String PREF_KEY_SETTING_ABOUTE_US = "PREF_KEY_SETTING_ABOUTE_US";
    private static final String PREF_KEY_SETTING_ABOUTE_US_SHORT = "PREF_KEY_SETTING_ABOUTE_US_SHORT";
    private static final String PREF_KEY_SETTING_CONTACT_US = "PREF_KEY_SETTING_CONTACT_US";
    private static final String PREF_KEY_SETTING_CONTACT_US_SHORT = "PREF_KEY_SETTING_CONTACT_US_SHORT";
    private static final String PREF_KEY_SETTING_RULES = "PREF_KEY_SETTING_RULES";


    private final SharedPreferences mPrefs;

    @Inject
    public AppPreferencesHelper(@ApplicationContext Context context,
                                @PreferenceInfo String prefFileName) {
        mPrefs = context.getSharedPreferences(prefFileName, Context.MODE_PRIVATE);
    }

    @Override
    public Long getCurrentUserId() {
        long userId = mPrefs.getLong(PREF_KEY_CURRENT_USER_ID, AppConstants.NULL_INDEX);
        return userId == AppConstants.NULL_INDEX ? null : userId;
    }

    @Override
    public void setCurrentUserId(String userId) {
        //  long id = userId == null ? AppConstants.NULL_INDEX : userId;
        //  mPrefs.edit().putLong(PREF_KEY_CURRENT_USER_ID, id).apply();
    }

    @Override
    public void setCurentUserWallet(String wallet) {
        mPrefs.edit().putString(PREF_KEY_CURRENT_USER_WALLET, wallet).apply();

    }

    @Override
    public String getCurrentUserName() {
        return mPrefs.getString(PREF_KEY_CURRENT_USER_NAME, null);
    }

    @Override
    public void setCurrentUserName(String userName) {
        mPrefs.edit().putString(PREF_KEY_CURRENT_USER_NAME, userName).apply();
    }

    @Override
    public String getCurrentUserEmail() {
        return mPrefs.getString(PREF_KEY_CURRENT_USER_EMAIL, null);
    }

    @Override
    public void setCurrentUserEmail(String email) {
        mPrefs.edit().putString(PREF_KEY_CURRENT_USER_EMAIL, email).apply();
    }

    @Override
    public String getCurrentUserProfilePicUrl() {
        return mPrefs.getString(PREF_KEY_CURRENT_USER_PROFILE_PIC_URL, null);
    }

    @Override
    public void setCurrentUserProfilePicUrl(String profilePicUrl) {
        mPrefs.edit().putString(PREF_KEY_CURRENT_USER_PROFILE_PIC_URL, profilePicUrl).apply();
    }

    @Override
    public void setSetting_Title(String title) {
        mPrefs.edit().putString(PREF_KEY_SETTING_TITLE, title).apply();
    }

    @Override
    public void setSetting_aboute_us(String abouteUs) {
        mPrefs.edit().putString(PREF_KEY_SETTING_ABOUTE_US, abouteUs).apply();
    }

    @Override
    public void setSetting_aboute_us_short(String aboute_us_short) {
        mPrefs.edit().putString(PREF_KEY_SETTING_ABOUTE_US_SHORT, aboute_us_short).apply();
    }

    @Override
    public void setSetting_contact_us(String contact_us) {
        mPrefs.edit().putString(PREF_KEY_SETTING_CONTACT_US, contact_us).apply();
    }

    @Override
    public void setSetting_contact_us_short(String contact_us_short) {
        mPrefs.edit().putString(PREF_KEY_SETTING_CONTACT_US_SHORT, contact_us_short).apply();
    }

    @Override
    public void setSetting_Rules(String rules) {
        mPrefs.edit().putString(PREF_KEY_SETTING_RULES, rules).apply();
    }

    @Override
    public String getSetting_Title() {
        return mPrefs.getString(PREF_KEY_SETTING_TITLE, null);
    }

    @Override
    public String getSetting_aboute_us() {
        return mPrefs.getString(PREF_KEY_SETTING_ABOUTE_US, null);
    }

    @Override
    public String getSetting_aboute_us_short() {
        return mPrefs.getString(PREF_KEY_SETTING_ABOUTE_US_SHORT, null);
    }

    @Override
    public String getSetting_contact_us() {
        return mPrefs.getString(PREF_KEY_SETTING_CONTACT_US, null);
    }

    @Override
    public String getSetting_contact_us_short() {
        return mPrefs.getString(PREF_KEY_SETTING_CONTACT_US_SHORT, null);
    }

    @Override
    public String getSetting_Rules() {
        return mPrefs.getString(PREF_KEY_SETTING_RULES, null);
    }

    @Override
    public void setCurentName(String name) {
        mPrefs.edit().putString(PREF_KEY_CURRENT_NAME, name).apply();
    }

    @Override
    public String getReagent_code() {
        return mPrefs.getString(PREF_KEY_CURRENT_USER_REAGETNT_CODE, null);
    }

    @Override
    public void setReagent_code(String reagent_code) {
        mPrefs.edit().putString(PREF_KEY_CURRENT_USER_REAGETNT_CODE, reagent_code).apply();
    }

    @Override
    public void setCurentUserFamily(String family) {
        mPrefs.edit().putString(PREF_KEY_CURRENT_USER_FAMILY, family).apply();
    }

    @Override
    public void setCurentUserPhoneNumber(String phoneNumber) {
        mPrefs.edit().putString(PREF_KEY_CURRENT_USER_PHONENUMBER, phoneNumber).apply();

    }

    @Override
    public void setCurentUserAddress(String address) {
        mPrefs.edit().putString(PREF_KEY_CURRENT_USER_ADDRESS, address).apply();

    }

    @Override
    public void setCurentUserTell(String tell) {
        mPrefs.edit().putString(PREF_KEY_CURRENT_USER_TELL, tell).apply();

    }

    @Override
    public void setCurentUserPostalCode(String postalCode) {
        mPrefs.edit().putString(PREF_KEY_CURRENT_USER_POSTALCODE, postalCode).apply();
    }

    @Override
    public void setCurentUserNational_Id(String national_id) {
        mPrefs.edit().putString(PREF_KEY_CURRENT_USER_National_Id, national_id).apply();
    }

    @Override
    public void setCurentUserBirthDay(String birthDay) {
        mPrefs.edit().putString(PREF_KEY_CURRENT_USER_BIRTHDAY, birthDay).apply();
    }

    @Override
    public String getCurentUserNational_Id() {
        return mPrefs.getString(PREF_KEY_CURRENT_USER_National_Id, null);

    }

    @Override
    public String getCurentUserBirthDay() {
        return mPrefs.getString(PREF_KEY_CURRENT_USER_BIRTHDAY, null);
    }

    @Override
    public void setCurentUserPass(String password) {
        mPrefs.edit().putString(PREF_KEY_CURRENT_USER_PASSWORD, password).apply();
    }

    @Override
    public String getCurentName() {
        return mPrefs.getString(PREF_KEY_CURRENT_NAME, null);
    }

    @Override
    public String getCurentUserFamily() {
        return mPrefs.getString(PREF_KEY_CURRENT_USER_FAMILY, null);
    }

    @Override
    public String getCurentUserWallet() {
        return mPrefs.getString(PREF_KEY_CURRENT_USER_WALLET, null);
    }

    @Override
    public String getCurentUserPhoneNumber() {
        return mPrefs.getString(PREF_KEY_CURRENT_USER_PHONENUMBER, null);
    }

    @Override
    public String getCurentUserAddress() {
        return mPrefs.getString(PREF_KEY_CURRENT_USER_ADDRESS, null);
    }

    @Override
    public String getCurentUserTell() {
        return mPrefs.getString(PREF_KEY_CURRENT_USER_TELL, null);
    }

    @Override
    public String getCurentUserPostalCode() {
        return mPrefs.getString(PREF_KEY_CURRENT_USER_POSTALCODE, null);
    }

    @Override
    public String getCurentUserPass() {
        return mPrefs.getString(PREF_KEY_CURRENT_USER_PASSWORD, null);
    }

    @Override
    public int getCurrentUserLoggedInMode() {
        return mPrefs.getInt(PREF_KEY_USER_LOGGED_IN_MODE,
                DataManager.LoggedInMode.LOGGED_IN_MODE_LOGGED_OUT.getType());
    }

    @Override
    public void setCurrentUserLoggedInMode(DataManager.LoggedInMode mode) {
        mPrefs.edit().putInt(PREF_KEY_USER_LOGGED_IN_MODE, mode.getType()).apply();
    }

    @Override
    public String getAccessToken() {
        return mPrefs.getString(PREF_KEY_ACCESS_TOKEN, null);
    }

    @Override
    public void setAccessToken(String accessToken) {
        mPrefs.edit().putString(PREF_KEY_ACCESS_TOKEN, accessToken).apply();
    }

    @Override
    public void setFireBaseToken(String firebase_token) {
        mPrefs.edit().putString(PREF_KEY_ACCESS_FIREBASETOKEN, firebase_token).apply();
    }

    @Override
    public String getFireBaseToken() {
        return mPrefs.getString(PREF_KEY_ACCESS_FIREBASETOKEN, null);
    }


}