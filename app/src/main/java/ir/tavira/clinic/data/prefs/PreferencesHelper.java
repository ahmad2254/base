/*
 * Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://mindorks.com/license/apache-v2
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package ir.tavira.clinic.data.prefs;


import ir.tavira.clinic.data.DataManager;

/**
 * Created by janisharali on 27/01/17.
 */

public interface PreferencesHelper {

    void setSetting_Title(String title);

    void setSetting_aboute_us(String abouteUs);

    void setSetting_aboute_us_short(String aboute_us_short);

    void setSetting_contact_us(String contact_us);

    void setSetting_contact_us_short(String contact_us_short);

    void setSetting_Rules(String rules);


    String getSetting_Title();

    String getSetting_aboute_us();

    String getSetting_aboute_us_short();

    String getSetting_contact_us();

    String getSetting_contact_us_short();

    String getSetting_Rules();

    void setCurentName(String name);
    String getReagent_code();

    void setReagent_code(String reagent_code);

    void setCurentUserFamily(String family);

    void setCurentUserPhoneNumber(String phoneNumber);

    void setCurentUserAddress(String phoneNumber);

    void setCurentUserTell(String tell);

    void setCurentUserPostalCode(String postalCode);

    void setCurentUserNational_Id(String national_id);
    void setCurentUserBirthDay(String birthDay);

    String getCurentUserNational_Id();
    String getCurentUserBirthDay();

    void setCurentUserPass(String password);

    /////
    String getCurentName();

    String getCurentUserFamily();
    String getCurentUserWallet();

    String getCurentUserPhoneNumber();

    String getCurentUserAddress();

    String getCurentUserTell();

    String getCurentUserPostalCode();

    String getCurentUserPass();


    ///
    int getCurrentUserLoggedInMode();

    void setCurrentUserLoggedInMode(DataManager.LoggedInMode mode);

    Long getCurrentUserId();


    void setCurrentUserId(String userId);
    void setCurentUserWallet(String wallet);

    String getCurrentUserName();

    void setCurrentUserName(String userName);

    String getCurrentUserEmail();

    void setCurrentUserEmail(String email);

    String getCurrentUserProfilePicUrl();

    void setCurrentUserProfilePicUrl(String profilePicUrl);

    String getAccessToken();

    void setAccessToken(String accessToken);
    void setFireBaseToken(String firebase_token);
    String getFireBaseToken();
}
