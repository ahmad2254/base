/*
 * Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://mindorks.com/license/apache-v2
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package ir.tavira.clinic.utils;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.text.Html;

import ir.tavira.clinic.R;


/**
 * Created by janisharali on 24/05/17.
 */

public final class AppUtils {

    private AppUtils() {
        // This class is not publicly instantiable
    }

    public static void openPlayStoreForApp(Context context) {
        final String appPackageName = context.getPackageName();
        try {
            context.startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse(context
                            .getResources()
                            .getString(R.string.app_market_link) + appPackageName)));
        } catch (android.content.ActivityNotFoundException e) {
            context.startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse(context
                            .getResources()
                            .getString(R.string.app_google_play_store_link) + appPackageName)));
        }
    }

    public static void openShareText(Context context, String s) {
       String content =
                "<p> با تیپ چین دیگر نیازی نیست از وقت کار یا لذت در کنار خانواده بودن کم کنید،آنلاین درخواست آرایشگر کنید تا آرایشگر در محلی که انتخاب میکنید حاضر شود\n\n" +
                "<a href=\"http://tipchin.com/app/tipchin.apk\">دانلود نرم افزار</a>\n" +
                 "<li>کد معرف  </li>\n"
                  + s;
        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
        sharingIntent.setType("text/html");
        sharingIntent.putExtra(Intent.EXTRA_SUBJECT, "اشتراک گذاری");
        sharingIntent.putExtra(Intent.EXTRA_TEXT, Html.fromHtml(content));
        context.startActivity(Intent.createChooser(sharingIntent, context.getResources().getString(R.string.share_using)));
    }


}
