package ir.tavira.clinic.utils;


import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.preference.PreferenceManager;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.TimeZone;

import ir.tavira.clinic.entity.DayEntity;
import ir.tavira.clinic.entity.date.CivilDate;
import ir.tavira.clinic.entity.date.Constants;
import ir.tavira.clinic.entity.date.DateConverter;
import ir.tavira.clinic.entity.date.DayOutOfRangeException;
import ir.tavira.clinic.entity.date.PersianDate;

import static ir.tavira.clinic.entity.date.Constants.ARABIC_DIGITS;
import static ir.tavira.clinic.entity.date.Constants.DEFAULT_PERSIAN_DIGITS;
import static ir.tavira.clinic.entity.date.Constants.PERSIAN_DIGITS;
import static ir.tavira.clinic.entity.date.Constants.PREF_PERSIAN_DIGITS;


/**
 * Common utilities that needed for this calendar
 *
 * @author ebraminio
 */

public class Utils {

    private final String TAG = Utils.class.getName();
    private Context context;
    private Typeface typeface;
    private SharedPreferences prefs;
    private String[] persianMonths;
    private String[] islamicMonths;
    private String[] gregorianMonths;
    private String[] weekDays;
    private char[] preferredDigits;
    private String cachedCityKey = "";

    private Utils(Context context) {
        this.context = context;
        prefs = PreferenceManager.getDefaultSharedPreferences(context);
        preferredDigits = isPersianDigitSelected()
                ? PERSIAN_DIGITS
                : ARABIC_DIGITS;
    }

    private static WeakReference<Utils> myWeakInstance;

    public static Utils getInstance(Context context) {
        if (myWeakInstance == null || myWeakInstance.get() == null) {
            myWeakInstance = new WeakReference<>(new Utils(context.getApplicationContext()));
        }
        return myWeakInstance.get();
    }

    public boolean isPersianDigitSelected() {
        return prefs.getBoolean(PREF_PERSIAN_DIGITS, DEFAULT_PERSIAN_DIGITS);
    }

    public List<DayEntity> getDays(int offset) {
        List<DayEntity> days = new ArrayList<>();
        PersianDate persianDate = getToday();
        int month = persianDate.getMonth() - offset;
        month -= 1;
        int year = persianDate.getYear();

        year = year + (month / 12);
        month = month % 12;
        if (month < 0) {
            year -= 1;
            month += 12;
        }
        month += 1;
        persianDate.setMonth(month);
        persianDate.setYear(year);
        persianDate.setDayOfMonth(1);

        int dayOfWeek = DateConverter.persianToCivil(persianDate).getDayOfWeek() % 7;

        try {
            PersianDate today = getToday();
            for (int i = 1; i <= 31; i++) {
                persianDate.setDayOfMonth(i);

                DayEntity dayEntity = new DayEntity();
                dayEntity.setNum(formatNumber(i));
                dayEntity.setDayOfWeek(dayOfWeek);
                dayEntity.setDayOfWeekp(Constants.FIRST_CHAR_OF_DAYS_OF_WEEK_NAME[dayOfWeek]);
                dayEntity.setEvent(false);
                dayEntity.setPersianDate(persianDate.clone());

                if (persianDate.equals(today)) {
                    dayEntity.setToday(true);
                }

                days.add(dayEntity);
                dayOfWeek++;
                if (dayOfWeek == 7) {
                    dayOfWeek = 0;
                }
            }
        } catch (DayOutOfRangeException e) {
            // okay, it was expected
        }

        return days;
    }

    public static String getDayOfWeek(Date d) {
        Calendar c = new GregorianCalendar();
        c.setTime(d);
        switch (c.get(Calendar.DAY_OF_WEEK)) {
            case 1:
                return "یکشنبه";
            case 2:
                return "دوشنبه";
            case 3:
                return "سه شنبه";
            case 4:
                return "چهارشنبه";
            case 5:
                return "پنج شنبه";
            case 6:
                return "جمعه";
            case 7:
                return "شنبه";
        }
        return null;
    }

    public PersianDate getToday() {
        return DateConverter.civilToPersian(new CivilDate(makeCalendarFromDate(new Date())));
    }

    public Calendar makeCalendarFromDate(Date date) {
        Calendar calendar = Calendar.getInstance();

        calendar.setTimeZone(TimeZone.getTimeZone("Asia/Tehran"));

        calendar.setTime(date);
        return calendar;
    }

    public String formatNumber(int number) {
        return formatNumber(Integer.toString(number));
    }

    public String formatNumber(String number) {
        if (preferredDigits == ARABIC_DIGITS)
            return number;

        char[] result = number.toCharArray();
        for (int i = 0; i < result.length; ++i) {
            char c = number.charAt(i);
            if (Character.isDigit(c))
                result[i] = preferredDigits[Character.getNumericValue(c)];
        }
        return String.valueOf(result);

    }

    public boolean phoneNumberValidation(String phonenumber) {
        return phonenumber.matches("(\\+98|0)?9\\d{9}");
    }

    public boolean codemelliValidation(String melliCode) {
        String[] identicalDigits = {"0000000000", "1111111111", "2222222222", "3333333333", "4444444444", "5555555555", "6666666666", "7777777777", "8888888888", "9999999999"};
        if (melliCode.trim().isEmpty()) {
           return false; // Melli Code is empty
        } else if (melliCode.length() != 10) {
           return false; // Melli Code is less or more than 10 digits
        } else if (Arrays.asList(identicalDigits).contains(melliCode)) {
            return false; // Fake Melli Code
        } else {
            int sum = 0;
            for (int i = 0; i < 9; i++) {
                sum += Character.getNumericValue(melliCode.charAt(i)) * (10 - i);
            }
            int lastDigit;
            int divideRemaining = sum % 11;
            if (divideRemaining < 2) {
                lastDigit = divideRemaining;
            } else {
                lastDigit = 11 - (divideRemaining);
            }
            if (Character.getNumericValue(melliCode.charAt(9)) == lastDigit) {
                return true;
            } else {
                return false; // Invalid MelliCode
            }
        }
    }
}
