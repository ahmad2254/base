/*
 * Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://mindorks.com/license/apache-v2
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package ir.tavira.clinic.ui.MainActivity.Main;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;
import ir.tavira.clinic.data.DataManager;
import ir.tavira.clinic.ui.base.BasePresenter;
import ir.tavira.clinic.utils.rx.SchedulerProvider;

/**
 * Created by janisharali on 25/05/17.
 */

public class MainFragmentPresenter<V extends MainFragmentMvpView> extends BasePresenter<V>
        implements MainFragmentMvpPresenter<V> {
    @Inject
    public MainFragmentPresenter(DataManager dataManager,
                                 SchedulerProvider schedulerProvider,
                                 CompositeDisposable compositeDisposable) {
        super(dataManager, schedulerProvider, compositeDisposable);
    }

    @Override
    public void onViewInitialized() {


    }

    @Override
    public void onChangeFragment()
    {

    }
}
