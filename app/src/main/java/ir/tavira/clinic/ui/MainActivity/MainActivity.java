package ir.tavira.clinic.ui.MainActivity;


import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import javax.inject.Inject;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import butterknife.ButterKnife;
import ir.tavira.clinic.R;
import ir.tavira.clinic.enums.Fragments;
import ir.tavira.clinic.ui.MainActivity.Main.MainFragment;
import ir.tavira.clinic.ui.base.BaseActivity;

public class MainActivity extends BaseActivity implements MainMvpView, View.OnClickListener {
    public static Intent getStartIntent(Context context) {
        Intent intent = new Intent(context, MainActivity.class);
        return intent;
    }

    @Inject
    MainPresenter<MainMvpView> mPresenter;


    public static Fragment main = MainFragment.newInstance();

    @SuppressLint("ResourceType")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getActivityComponent().inject(this);
        setUnBinder(ButterKnife.bind(this));
        mPresenter.onAttach(this);
        setUp();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:

                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @SuppressLint("WrongConstant")
    @Override
    protected void setUp() {
        mPresenter.onViewInitialized();
        mPresenter.onChangeFragment(Fragments.MAINN);
    }


    @Override
    public void onShowMainFragment() {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        if (main.isAdded()) { // if the fragment is already in container
            ft.show(main);
        } else { // fragment needs to be added to frame container
            ft.add(R.id.cl_root_view, main, MainFragment.TAG);
        }

        ft.commit();
    }

    @Override
    public void openActivityOnTokenExpire() {
        mPresenter.getDataManager().setUserAsLoggedOut();
        onShowMainFragment();
        onError("اطلاعات کاربری شما منقضی شده است لطفا مجدد وارد سیستم شوید.");
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
        }
    }
}