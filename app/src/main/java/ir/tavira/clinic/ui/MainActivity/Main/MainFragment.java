/*
 * Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://mindorks.com/license/apache-v2
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package ir.tavira.clinic.ui.MainActivity.Main;

import android.annotation.SuppressLint;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import javax.inject.Inject;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import butterknife.BindView;
import butterknife.ButterKnife;
import ir.tavira.clinic.R;
import ir.tavira.clinic.di.component.ActivityComponent;
import ir.tavira.clinic.ui.base.BaseFragment;


/**
 * Created by janisharali on 25/05/17.
 */

public class MainFragment extends BaseFragment implements
        MainFragmentMvpView, View.OnClickListener {

    public static final String TAG = "MainFragment";

    @Inject
    MainFragmentMvpPresenter<MainFragmentMvpView> mPresenter;


    @Override
    public void onResume() {
        super.onResume();
        // register GCM registration complete receiver

    }

    public static MainFragment newInstance() {
        Bundle args = new Bundle();
        MainFragment fragment = new MainFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main, container, false);

        ActivityComponent component = getActivityComponent();
        if (component != null) {
            component.inject(this);
            setUnBinder(ButterKnife.bind(this, view));
            mPresenter.onAttach(this);
        }
        return view;
    }

    @SuppressLint("WrongConstant")
    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void setUp(View view) {
        mPresenter.onViewInitialized();
    }

    @Override
    public void onError(String message) {
        super.onError(message);
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        mPresenter.onDetach();
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {

        }
    }
}
