package ir.tavira.clinic;

import android.content.Context;
import android.content.res.Configuration;

import com.androidnetworking.AndroidNetworking;

import org.acra.ACRA;
import org.acra.ReportingInteractionMode;
import org.acra.annotation.ReportsCrashes;

import javax.inject.Inject;

import androidx.multidex.MultiDex;
import androidx.multidex.MultiDexApplication;
import ir.tavira.clinic.data.DataManager;
import ir.tavira.clinic.di.component.ApplicationComponent;
import ir.tavira.clinic.di.component.DaggerApplicationComponent;
import ir.tavira.clinic.di.module.ApplicationModule;
import ir.tavira.clinic.utils.AppLogger;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

/**
 * @author : hafiq on 23/01/2017.
 */
@ReportsCrashes(mailTo = "ahmadsajadi73@gmail.com", // ایمیلی که می خواهید لاگ کت کرش به آن ارسال شود.
        mode = ReportingInteractionMode.DIALOG, // ارور به شکل دیالوگ نمایش داده میشود.
        resDialogText = R.string.crash_report, // عنوان دیالوگ
        resDialogTitle = R.string.crash_title, // متن دیالوگ
        resDialogTheme = R.style.AppCompatAlertDialogStyle, // استایل دیالوگ
        resDialogIcon = R.mipmap.ic_launcher)

public class App extends MultiDexApplication {
    @Inject
    DataManager mDataManager;

    @Inject
    CalligraphyConfig mCalligraphyConfig;

    private ApplicationComponent mApplicationComponent;





    @Override
    public void onCreate() {
        super.onCreate();
        mApplicationComponent = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this)).build();

        mApplicationComponent.inject(this);

        AppLogger.init();

        AndroidNetworking.initialize(getApplicationContext());
//        AndroidNetworking.setParserFactory(new JacksonParserFactory());
        if (BuildConfig.DEBUG) {
            AndroidNetworking.enableLogging(com.androidnetworking.interceptors.HttpLoggingInterceptor.Level.BODY);
        }
        MultiDex.install(this);
        CalligraphyConfig.initDefault(mCalligraphyConfig);
        ACRA.init(this); // فعال کردن دیالوگ
        }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }


    public static App getApp(Context c) {
        return (App) c.getApplicationContext();
    }
    @Override
    protected void attachBaseContext(Context base)
    {
        super.attachBaseContext(base);
        MultiDex.install(base);
    }

    public ApplicationComponent getComponent() {
        return mApplicationComponent;
    }


    // Needed to replace the component with a drawer specific one
    public void setComponent(ApplicationComponent applicationComponent) {
        mApplicationComponent = applicationComponent;
    }

}
