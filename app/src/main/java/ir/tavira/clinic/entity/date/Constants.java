package ir.tavira.clinic.entity.date;

public class Constants {
    public final static String DAY = "day";
    public final static String IS_OUT_OF_RANGE = "is out of range!";
    public final static String NOT_IMPLEMENTED_YET = "not implemented yet!";
    public final static String MONTH = "month";
    public final static String YEAR_0_IS_INVALID = "Year 0 is invalid!";


    public static final String PREF_PERSIAN_DIGITS = "PersianDigits";
    public static final boolean DEFAULT_PERSIAN_DIGITS = true;
    public static final String[] FIRST_CHAR_OF_DAYS_OF_WEEK_NAME = { "شنبه" , "یکشنبه" , "دوشنبه" , "سه شنبه" ,
            "چهارشنبه" , "پنجشنبه" , "جمعه" };
    public static final char[] ARABIC_DIGITS = {'0', '1', '2', '3', '4', '5', '6',
            '7', '8', '9'};
    public static final char[] PERSIAN_DIGITS = {'۰', '۱', '۲', '۳', '۴', '۵', '۶',
            '۷', '۸', '۹'};
    public static final String OFFSET_ARGUMENT = "OFFSET_ARGUMENT";
}
