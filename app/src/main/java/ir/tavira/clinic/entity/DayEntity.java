package ir.tavira.clinic.entity;

import ir.tavira.clinic.entity.date.PersianDate;

public class DayEntity {
    private String num;
    private boolean holiday;
    private boolean today;
    private int dayOfWeek;
    private String dayOfWeekp;
    private PersianDate persianDate;
    private boolean event;

    public boolean isEvent() {
        return event;
    }

    public void setEvent(boolean event) {
        this.event = event;
    }

    public String getNum() {
        return num;
    }

    public void setNum(String num) {
        this.num = num;
    }

    public boolean isHoliday() {
        return holiday;
    }

    public void setHoliday(boolean holiday) {
        this.holiday = holiday;
    }

    public boolean isToday() {
        return today;
    }

    public void setToday(boolean today) {
        this.today = today;
    }

    public int getDayOfWeek() {
        return dayOfWeek;
    }

    public String getDayOfWeekp() {
        return dayOfWeekp;
    }

    public void setDayOfWeekp(String dayOfWeekp) {
        this.dayOfWeekp = dayOfWeekp;
    }

    public void setDayOfWeek(int dayOfWeek) {
        this.dayOfWeek = dayOfWeek;
    }

    public PersianDate getPersianDate() {
        return persianDate;
    }

    public void setPersianDate(PersianDate persianDate) {
        this.persianDate = persianDate;
    }
}
